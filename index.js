const express = require('express')

const app = express()

app.set('view engine', 'ejs')
app.use('/assets', express.static('public'))

app.get('/', function(request, response){
    const data = {
        title: 'Home | Alfian Taufani',
        subtitle: 'Home',
    }
    response.render('pages/index', {data: data})
})
app.get('/about', function(request, response){
    response.send('About')
})

app.get('/users', function(request, response){
    response.send('Get User')
})
app.post('/users', function(request, response){
    response.send('Post User')
})
app.put('/users/:id', function(request, response){
    const id = request.params
    response.send(id)
})
app.delete('/users', function(request, response){
    response.send('Delete User')
})

app.listen(3000, function(){
    console.log('server is okey')
})